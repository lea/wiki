[[!table header="no" class="mointable" data="""
 [[Home|FrontPage]]  |  [[TiNDC 2006|IrcChatLogs]]  |  [[TiNDC 2007|IrcChatLogs]]  |  [[Old Logs|IrcChatLogs]]  |  [[Current Logs|IrcChatLogs]]  | DE/EN/[[ES|Nouveau_Companion_21-es]]/[[FR|Nouveau_Companion_21-fr]]/RU/[[Team|Translation_Team]] 
"""]]


## The irregular Nouveau-Development companion


## Issue for June, 10th


### Intro

Hello again to issue number 21 of the TiNDC. 

Before we begin, I would like to hand out a thank you to all of our contributers and testers of our project. Not only the developers but you too are an important part of our project. Special thanks to the members of our translation team who even take on the task to translate the TiNDCs just in time with the publication of the english issue. 

ABClinux did an interview with Andy Ritger, Director of Unix Software at  NVIDIA. One question was about nouveau and what NVidia intends to do about us: 

* NVIDIA's stance is to neither help nor hinder Nouveau. We are committed  to supporting Linux through a) an open source 2d "nv" X driver which NVIDIA  engineers actively maintain and improve, and b) our fully featured proprietary  Linux driver which leverages common code with the other platforms that NVIDIA  supports. 
Read the full interview [[here|http://www.abclinuxu.cz/clanky/rozhovory/andy-ritger-nvidia?page=1]]. 

Seems to us that this is the best we can expect right now. 

Regarding the SFC and therefore the 10.000 USD donation. The meeting was held and the  board members were not sure whether to accept us or not. They asked for more  technical information from Marcheu, who is now in the process of writing and submitting them. Hopefully we will get a final decision soon. 

Finally we found out that we have space alotted since we moved here on the fd.o servers and promptly moved all of the sf.net dumps over to [[fd.o|http://nouveau.freedesktop.org/tests/]]. Currently we  are waiting for an account for the script run by kmeyer to do the uploads automatically. But this shouldn't take much longer. 


### Current status

Now to the part I guess most of you are interested in, the technical progress part. 

ahuillet continues to collect important data about Xv and its technical requirements. He already wrote or linked to some pages about it in the Wiki [[here|ArthurHuillet]] and [[here|http://nouveau.freedesktop.org/wiki/XvMC]]. He currently plans to  start work around June 21th and has already promised to keep you informed about his  progress in the TiNDCs which are going to be published throughout the time frame of  his project. 

We are still fighting with the 8600 GT based cards on which the project managed  to get its hands on. Most work is driven by Darktama with some support from  KoalaBR. 

KoalaBR's card now works in 2D with darktama's nv50_branch. Some trivial omissions were fixed (but are not yet pushed to the public repository, see below for the reasons). 

Returning to text mode does interesting things but text mode is none of them :) So KoalaBR tried to steal code from NV using the int10 code to call the BIOS. Marcheu stopped this because stealing is immoral and scolded KoalaBR for trying. (Well to be honest, the reason is more technical: It is simply non portable to other architectures than Linux x86 and x86-64). 

airlied offered an enhanced vbetool which would optionally run BIOS calls via an emulator layer and print out all register accesses to the card. However it  didn't work with the emulator. So after some mulling over what to do, airlied  remembered another version of said tool and library, which would simply  printf() the needed data. Due some 24x7 tests for his job, a link appeared only a few days later. But still no luck, console font got corrupted and monitor went into suspend mode (no signal). Switching manually from text to X and back returns the display, but the font is still corrupted and needs to be reloaded. 

When talking with airlied and marcheu about further options, both remembered  a patch from NVidia to the Xorg x86emu. NVidia claimed it was necessary to get G8x cards working correctly. So next step is to try this version and see whether the tools will work. 

Goal is to find out the register accesses and to write a small tool which just writes to the registers itself and gets mode switching to work. When it works for the majority of G8x cards we will incorporate mode switching back into our driver. PLEASE NOTE: Although this work may be helpful for randr12 I just talked about switching from X back to text mode, as this is what I am trying to get working! 

Darktama thinks he has some of the 3D features like shaders addresses and virtual memory management per OpenGL client figured out.  Virtual Memory would be very useful for scatter / gather DMA access via PCI or PCI-e and unsurprisingly, he has a prototype TTM-backend to do scatter and gather DMA (this backend is not specific for G8x by the way). By the way, it seems as if the current G8x cards have 64 bit addressing while older cards only have 32 bit addressing. 

Finally, jkolb_ had a look at outstanding NV3x issues and got his card "working" with glxgears. However, the issue noted in [[TiNDC 15|Nouveau_Companion_15]]. (projection matrix not applied) knocked on his door, so he decided to have a look. He noted that none of our init functions for NV3x did send the needed data, so he offered to try to get out a patch. Marcheu confirmed that saying that the matrix is currently managed via shaders and thus only available on NV4x. When using the software version, a bugs appears which always applies the matrix twice or (as is currently) not at all. Marcheu claimed patent protection on this very innovative feature and promised to have a look at it next week. 

With marcheu back in action after entering his thesis starting next week, we  should see more progress on other architectures too. 


### Help needed

Well, we have the usual supects here: renouveau and mmio dumps, additional tests of nouveau. 

Coders who want to help won't be rejected either :) 

[[<<< Previous Issue|Nouveau_Companion_20]] | [[Next Issue >>>|Nouveau_Companion_22]] 
