[[!table header="no" class="mointable" data="""
 [[Accueil|FrontPage-fr]]  |  [[TiNDC 2006|IrcChatLogs-fr]]  |  [[TiNDC 2007|IrcChatLogs-fr]]  |  [[TiNDC 2008|IrcChatLogs-fr]]  |  [[Archives anciennes|IrcChatLogs-fr]]  |  [[Archives récentes|IrcChatLogs-fr]]  | DE/[[EN|Nouveau_Companion_33]]/[[ES|Nouveau_Companion_33-es]]/[[FR|Nouveau_Companion_33-fr]]/RU/[[Team|Translation_Team]] 
"""]]


## Le compagnon irrégulier du développement de Nouveau (TiNDC)


## Édition du 12 janvier 2008


### Introduction

Nous souhaitons une bonne et heureuse année à tout nos lecteurs. Bienvenue à la première édition du TiNDC de cette nouvelle année. 

Je tiens à remercier tous nos contributeurs et testeurs pour leur travail durant l'année 2007. Nous espérons pouvoir voir certains d'entre vous durant le FOSDEM 2008. 

L'édition précédente fut discrète en raison des vacances et de mon travail. Le résultat, c'est que je l'ai publié sur le wiki quand probablement personne ne regardait. 

Une question est apparu sur IRC au sujet du fonctionnement de Nouveau sur NetBSD, à laquelle une réponse négative a été apportée. Un utilisateur BSD est apparu peu après pour nous informer de [[http://mail-index.netbsd.org/tech-x11/2007/04/25/0000.html|http://mail-index.netbsd.org/tech-x11/2007/04/25/0000.html]] (une ébauche de portage), dont nous étions totalement ignorants. Si des développeurs NetBSD lisent ce TiNDC : Nous sommes très intéressés par des patchs pour les *BSD et nous les intégrerons avec joie dans le code. Si quelque chose de plus récent existe, n'hésitez pas. :) 

JussiP a réparé la page de statut des dump REnouveau, vous devriez maintenant correctement voir ceux qui sont obsolètes. :) 


### État actuel

Du côté des machines PPC qui ne fonctionnaient pas : Marcheu a détecté et corrigé des bugs dans le DRM ( [[http://cgit.freedesktop.org/mesa/drm/commit/?id=cd19dcef4f7cc454f68618a0a1e903f159db21ad|http://cgit.freedesktop.org/mesa/drm/commit/?id=cd19dcef4f7cc454f68618a0a1e903f159db21ad]] ). À coté du code buggué de notre part, nous avions également un problème additionnel : l'accès au BIOS (NDT : de la carte graphique) après le démarrage ne se déroulait pas correctement, le BIOS montrant des signes de corruption (qui n'existait pas, puisqu'il fonctionnait au démarrage). Malc0 ayant eu le même problème l'année dernière, il avait été réglé en copiant le BIOS dans la RAM et en utilisant cette image (pour le parsing DCB etc). 

Néanmoins, nous avions toujours des retours avec ce problème, montrant qu'il était toujours présent plus ou moins aléatoirement. Marcheu a maintenant déplacé la copie le plus tôt possible durant le démarrage, espérons que le problème sera corrigé définitivement ([[http://cgit.freedesktop.org/mesa/drm/commit/?id=de522ae742bd058780135eb21fe287e9a9dc263a|http://cgit.freedesktop.org/mesa/drm/commit/?id=de522ae742bd058780135eb21fe287e9a9dc263a]]). 

AndrewR nous a fourni un correctif pour le contrôle de l'overlay qui a été nettoyé par Ahuillet ([[http://people.freedesktop.org/~ahuillet/irclogs/nouveau-2008-01-07.htm#1348|http://people.freedesktop.org/~ahuillet/irclogs/nouveau-2008-01-07.htm#1348]]). Il devrait permettre le contrôle de la luminosité et des couleurs avec Xv, ainsi que d'autres bonnes choses pour les NV04/05 (TNT/TNT2). 

Marcheu a également fait quelques corrections sur Xv pour xynchroniser le blitter avec la sortie video, de façon à éviter que les images coulent ("tearing") sur les NV40. 

Stillunknown a finalement entrepris de faire fonctionner Randr1.2 sur les cartes un peu anciennes (NV1x et NV2x). Avec Ahuillet, ils ont éliminé quelques problèmes. 

Quelques résultats de son travail et de patchs de malc0 durant les vacances : 

* - le DVI dual link devrait fonctionner, testeurs demandés - les 7300 GO sur LVDS ont été testés et mis à jour par stillunknown et seventhguardian mais les premiers essais n'ont pas donnés grand chose de concluant. [[!img http://people.freedesktop.org/~hughsient/temp/nouveau-split.jpg]. Après quelques jours de recherche, hughsie reporta le même problème, sur un système similaire. LVDS était donc officiellement cassé (ce qui est quand même mieux que le précédent statut « non supporté » !). Seventhguardian, avec l'aide de stillunknown, a réussi finalement à régler le problème en jouant aux essais/erreurs dans nv_crtc.c. - 7300 go a également reçu des corrections pour le modesetting Randr1.2. 
Comme ça ne suffisait pas, stillunknown a tenté sa chance pour la lecture de vidéo par le biais de textures (via les shaders) sur NV4x. Le blitter d'Ahuillet marche très bien, mais il est tout de même un peu lent et il y a de la place pour des améliorations si on pouvait utiliser les shaders. 

Darktame et thunderbird expliquèrent les basiques à stillunknown, et après un peu de travail, il parvint à faire fonctionner un test simple. Quelques jours plus tard, l'adaptateur yv12 en niveaux de gris fonctionnait. Le rythme a un peu ralenti par la suite, stillunknown rencontrant des problèmes à comprendre le code des shaders. Il posta son code pour revue, laquelle fut faite par thunderbird et Marcheu. 

Le code corrigé d'après les remarques, un jour plus tard, les couleurs devinrent fonctionnelles. Même si le filtrage bilinéaire posait toujours problème ([[http://people.freedesktop.org/~marcheu/irclogs/nouveau-2007-12-29#1217|http://people.freedesktop.org/~marcheu/irclogs/nouveau-2007-12-29#1217]]). 

Un peu plus tard, Ahuillet améliora la qualité d'image du blitter à l'aide d'une interpolation linéaire dans la conversion YV12->YUY2 ([[http://people.freedesktop.org/~ahuillet/irclogs/nouveau-2008-01-04.htm#2134|http://people.freedesktop.org/~ahuillet/irclogs/nouveau-2008-01-04.htm#2134]]). 

Et étant en mode code, il a ajouté une autre amélioration à Xv : l'overlay marche maintenant un peu mieux avec les configuration bi-écrans. Il bascule de l'un à l'autre correctement et se rabat sur le blitter pour que vous n'ayez jamais une fenêtre bleue à la place de la vidéo. ([[http://people.freedesktop.org/~ahuillet/irclogs/nouveau-2008-01-06.htm#1547|http://people.freedesktop.org/~ahuillet/irclogs/nouveau-2008-01-06.htm#1547]]) 

Quelques nouvelles rapides : 

* Ahuillet a renoncé à faire fonctionner l'opération A8+A8 sur PPC et l'a désactivé (tout au moins, il l'a exprimé :) ) 
* AndrewR et fsteinel_ ont rapporté des problèmes avec les TNT2. andrewR a trouvé le commit en cause. C'était un problème avec [[ImageFromCpu|ImageFromCpu]]. Il changea NV05_IMAGE_FROM_CPU pour NV_IMAGE_FROM_CPU, ce qui sembla avoir réglé le soucis. 
* Malc0 a résolu des problèmes de crashs sur les NV30 en désactivant puis réactivant l'AGP dans le DRM, le patch a été ensuite nettoyé et commité par stillunknown.([[http://people.freedesktop.org/~ahuillet/irclogs/nouveau-2007-12-28.htm#1353|http://people.freedesktop.org/~ahuillet/irclogs/nouveau-2007-12-28.htm#1353]]) 
* Marcheu continue à retravailler le code Xv. Au final, il devrait en résulter un adaptateur video générique qui basculera vers le bon (overlay, blitter, texture etc) à la volée. 
* Marcheu a également réalisé « sync to vblank » ainsi que différents travaux (filtrage, optimisations etc) pour l'adaptateur texture de xv, travail toujours en cours. 
* Ahuillet a reçu un rapport d'AndrwR (l'homme aux nombreuses cartes :) ) comme quoi sa NV05 ne fonctionnait pas. À première vue, Ahuillet était sceptique mais il trouva des preuves dans les logs d'AndrewR. Étonnament, la carte NV05 semblait utiliser des méthodes logicielles qu'elle n'aurait pas du. Fait nouveau qui décida Ahuillet à enquêter sur ce problème. Quelques jours plus tard, darktama trouva le problème. (IRC : [[http://people.freedesktop.org/~ahuillet/irclogs/nouveau-2008-01-11.htm#0248|http://people.freedesktop.org/~ahuillet/irclogs/nouveau-2008-01-11.htm#0248]]). Un commit avait conduit à effacer le bit qui ordonnait à la carte de gérer les méthodes logicielles par elle même, résultant en une tempête d'interruptions. Une fois le bit remis en place, tout fonctionna. 
* jkolb a ajouté le context voodoo pour les cartes NV86. 
* Le travail de marcheu sur le framework Gallium pour les cartes anciennes est plus ou moins en attente en raison des contraintes de la vie réelle. 
Sur les noyaux récents (>= 2.6.24), le notificateur d'erreur de pages mémoires a disparu et donc, MMioTrace ne peut plus fonctionner. PQ demanda de l'aide sur la LKML ([[http://marc.info/?t=119982207100002&r=1&w=2|http://marc.info/?t=119982207100002&r=1&w=2]]) mais reçu une réponse négative. Donc pour le moment, si vous voulez utiliser MMioTrace, cantonnez vous aux noyaux < 2.6.24. Pq cherche à régler le problème. Simultanément, airlied, benh et d'autres vinrent apporter leur soutien à pq sur la LKML, demandant à ce que le patch soit retiré, ou qu'au moins, une fonction similaire soit disponible. Au final, il semble que pq va essayer d'inclure mmiotrace dans la version 2.6.25 et il devrait obtenir de l'aide des gourous du noyau pour trouver des remplaçantes aux fonctions manquantes. 

Finalement, nous avons plusieurs retours comme quoi l'architecture PPC fonctionne. Si vous êtes un utilisateur de cette dernière, n'oubliez pas de tester pour éviter toute régression. 


### Aide requise

Les utilisateurs de NV4x peuvent tester l'adaptateur video texture et s'adresser à stillunknown pour les retours (NDT : c'est le mode par défaut lorsque vous utilisez xv avec mplayer). 

Les possesseurs de NV04/05 peuvent tester le code actuel pour nous dire s'il fonctionne ou non. 

Le code Randr1.2 changeant souvent, il faut donc réaliser des tests régulièrement ! Adressez vous à malc0 ou stillunknown si vous trouvez des problèmes. 

Et bien sur, comme toujours, jetez un oeil sur la page [[TestersWanted|TestersWanted]] pour savoir de quoi nous avons besoin. 

[[<<Édition précédente|Nouveau Companion 32-fr]] [[Édition suivante >>|Nouveau_Companion_34-fr]] 
