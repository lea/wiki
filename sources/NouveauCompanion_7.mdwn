## The irregular Nouveau-Development companion


## Issue for November, 17th


### Intro

Once again it is time for a new TiNDC. I would like to say "Thank you" to all of you, who keep asking for a new version. It shows us, that you really like this summary. 

Furthermore, a "Thank you" to all of you, who drop in into `#nouveau` and offer either dumps or risk their system's integrity just to test the newest releases of nouveau. 


### Current status

Let's start with arlied and his quest for deobfuscation and dual-head support: He is waiting for xrandr++ to appear in the xorg tree. The newest version of "X11 Rotate and Resize" offers much functionality we would otherwise need to implement ourself ("mergefb" support). And all that would be obsolete because the new interface would be replacing much of the implemented functionality. So rest assured: As far as we can see, we will support dual-head but work on it won't start for another 2-6 months in the  worst case. Currently it looks like xrandr++ will appear in Xorg-Server 1.2.1. Above all that, arlied identified some endianess issues on his G5  PowerMac and fixed them right away. And finally he fixed some smaller errors like failing gracefully if loading of libdrm fails because there is already  one loaded. 

In a related topic, arlied attended an invitation only event, where some kernel and graphic hackers (Linus, Xorg, graphic driver writers etc.) met  and discussed the future of graphics in the kernel. Currently, Linux isn't very good when it comes to the management of graphical resources. Best example is the problematic interaction between frame buffers and X11 drivers. Basically both pretend to be the only user of the graphic card and the hackers need to jump through quite a bunch of loops when trying to get them both working. So the consensus is, that the kernel needs to have some knowledge about graphical resources and a way to "manage" them. Only how much knowledge the kernel needs resulted in many different opinions which prompted this meeting. The result is: There needs to be some interface for the kernel, which should be able to do basic mode setting and some locking in order to allow / disallow process access to a given resource. 

This was an important outcome for us too, as we now know which features we must support kernelwise. 

When the last TiNDC was published, the context switching was not working for cards better than NV2x. Darktama had it working for NV1x-NV2x (but the code is currently not available in git, as Darktama does not have daily access to the computer he developed and tested it on), but newer cards nearly immediately locked up. 

30.10.2006  
 `<jkolb> how is the init stuff going?`  
 `<marcheu> 5 hard lockups today - still no dice`   
 `<darktama> jkolb: painfully :)`  
 

12.11.2006  
 `<marcheu> hmm NV40_PFIFO_RAMFC is in no reg header yet ?`  
 `<darktama_> hm, no.. oops`  
 `<marcheu> it's 2220`  
 `<darktama_> yup`  
 `<marcheu> so you say 207fa`  
 `<marcheu> which would mean put it in RAMIN`  
 `<darktama_> yeah.. I'm not expecting it to work though :)`  
 

* . .  
`<marcheu> darktama_, I have to say you rock`  
 `<darktama_> it worked? `  
 `<marcheu> well nouveau_demo did work`   
 `<marcheu> and RAMFC was updated correctly`  
 `<darktama_> ooh, nice.. I wasn't expecting that`  
 `<marcheu> and no interrupt to be seen`  
 

And since pq and KoalaBR did verify this finding with a 256 MB and 128 MB NV43 card we are proud to announce that context switching finally works! 

**HOWEVER**: Don't ask for 3D acceleration just yet, as I did simplify the problem in my last TiNDC too much. In order to put the card to correct usage we need both context and graphic context switching. Now a context is simply the state of a fifo  with pointers where to `PUT` or `GET` commands while the graphics context is the state  of the various objects. And as each context has its own set of created objects, the card needs to be able to assign the correct objects to the correct context.  Unfortunately, the exact nature of the graphic context is currently not fully understood. But that seems to be the last major stumbling block before we can start to think about implementing 3D acceleration, so be patient please! (Or even better, offer your help!). 

For testing purposes we now have a small program which tries to setup context switches on a given NVidia card. PLEASE BEWARE: Please do check with either Marcheu or Darktama whether a run on your hardware would be helpful to our cause and if so, where to find it. It may result in Kernel oopses, X11 crashes or hard lockups! If you like to live on the bleeding edge, please do contact us on `#nouveau` first. Just to make sure that you don't run the program by mistake we require you to be root :) 

Furthermore, jkolb did some testing as well as some debug macros, which were sorely needed but neglected by me (I'm really sorry,  Darktama). Finally he helped deobfuscating some code inherited from "nv". 


### Help needed

We always need help, just drop into `#nouveau` on freenode.org and offer your help. Currently we would really like someone with the newly released NVidia 8800 to come to our channel and offer testing help. It seems as if some new objects were introduced with this new generation of cards. 

And just to cure my curiosity: Is there any way in this Wiki to find out how many times a certain page has been served? 

[[<<< Previous Issue|NouveauCompanion_6]] | [[Next Issue >>>|Nouveau_Companion_8]] 
